<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sale extends Model
{
	protected $primaryKey = 'sid';
	protected $table = 'sales';

	public function Details() {
		return $this->hasMany(saleDetail::class,'sid','sid');
	}

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
	}

	public function Payment() {
		return $this->hasOne(payment::class,'sid','sid');
	}
}
