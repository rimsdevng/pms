<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class saleDetail extends Model
{
    //

    protected $primaryKey = 'sdid';
    protected $table = 'sale_details';

	public function Product() {
		return $this->belongsTo(product::class,'pid','pid');
    }

	public function Sale() {
		return $this->belongsTo(sale::class,'sid','sid');
  }
}
