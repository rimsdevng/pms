<?php

namespace App\Http\Controllers;


use App\category;
use App\product;
use App\User;
use App\sale;
use App\productImage;
use App\saleDetail;
use App\salesHistory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->middleware('auth');
		
		if(!session()->has('carttotal')) session()->put('carttotal',0);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     return view('home');
    // }

    public function index()
    {
		$products = product::all();
		$totalsales = saleDetail::all();
		// $users = User::where('role','user')->get();
		$users = User::all();
		$sales = category::all();
		// $todaysales = sale:: where( DB::raw('DATE(created_at)') ,'=', Carbon::today());
		
		$todaysales = sale::whereDate('created_at', Carbon::today())->count();

        return view('home',[
			'products' => $products,
			'totalsales' => $totalsales,
	        'users'    => $users,
			'sales'    => $sales,
			'todaysales' => $todaysales
        ]);
    }

	public function profile() {
		return view('frontend.account.profile');
    }

	public function checkout() {
		return view('backend.sales.print');
    }

	public function addCategory() {
		return view('backend.categories.add');
	}

	public function editCategory($catid) {

		$category = category::find($catid);

		return view('backend.categories.edit',[
			'category' => $category
		]);
	}

	public function deleteCategory($catid) {
		try{
			category::destroy($catid);
			session()->flash('success','Category Deleted.');

		}catch (\Exception $exception){
			session()->flash('error', 'An error occurred. Please try again.');
		}
		return redirect()->back();
	}

	public function postAddCategory(Request $request) {

		try{
			$name = $request->input('name');
			$category  = new category();
			$category->slug = str_slug($name);
			$category->name = $name;
			$category->description = $request->input('description');
			$category->save();

			session()->flash('success', 'Category added');

		}catch (\Exception $exception){

			session()->flash('error','Something went Wrong');

		}

		return redirect()->back();

	}


	public function postEditCategory( $catid ) {

    	try{

		    $category =  category::find($catid);

		    $category->update(request()->all());

		    session()->flash('success','Category Edited');

	    }catch (\Exception $exception){
		    session()->flash('error', 'something went wrong');
	    }


		return redirect()->back();
	}
	
	

	public function getCategories() {
		$categories = category::all();

		return view('backend.categories.manage',[
			'categories' => $categories
		]);
	}

	public function manageProducts() {
		// $products = product::all();

		$currentDate = date('Y-m-d');
		$products = product::where('expiry_date', '>=', $currentDate)->get();
	
		$categories = category::all();

		return view('backend.products.manage',[
			'products' => $products,
			'categories' => $categories
		]);
	}

	public function addProduct() {
    	$categories = category::all();
		return view('backend.products.add',[
			'categories' => $categories
		]);
	}

	public function editProduct($pid) {
		$product = product::find($pid);
		$categories = category::all();
		return view('backend.products.edit',[
			'product' => $product,
			'categories' => $categories

		]);

	}

	public function deleteProduct($pid) {

    	try{
		    product::destroy($pid);
		    session()->flash('success','Product Deleted');
	    }catch (\Exception $exception){
		    session()->flash('error','something went wrong');
	    }

		return redirect()->back();

	}

	public function postAddProduct(Request $request  ) {

		$name = $request->input('name');

	//	try{
			$product = new product();
			$product->name = $name;
			$product->catid = $request->input('catid');
			$product->price = $request->input('price');
			$product->manufacturer = $request->input('manufacturer');
			$product->made_in = $request->input('made_in');
			$product->components = $request->input('components');
			$product->key_features = $request->input('key_features');
			$product->instructions = $request->input('instructions');
			$product->slug = str_slug($name);
			// strtolower($request->input('batch_no'));
			$product->batch_no = $request->input('batch_no');
			$product->mfg_date = $request->input('mfg_date');
			$product->expiry_date = $request->input('expiry_date');
			//dimension

			$product->save();

			if($request->hasFile('images')){
			foreach ( $request->file( 'images' ) as $item ) {
				$rand = Carbon::now()->timestamp;
				$inputFileName = $item->getClientOriginalName();

				$item->move( "uploads", $rand . $inputFileName );
				$url = url("/uploads/" . $rand . $inputFileName);
				$productImage = new productImage();
				$productImage->pid = $product->pid;
				$productImage->url = $url;
				$productImage->save();

			}

		}


		session()->flash('success', 'Product Added Successfully');

		// }catch (\Exception $exception){
		// 	session()->flash('error', 'An error occurred. Please try again');
		// }

		return redirect()->back();
	}

	public function postEditProduct(Request $request, $pid) {

   	try{

			$name = $request->input('name');
		    $product = product::find($pid);
			$product->name = $name;
			$product->catid = $request->input('catid');
			$product->price = $request->input('price');
			$product->manufacturer = $request->input('manufacturer');
			$product->made_in = $request->input('made_in');
			$product->components = $request->input('components');
			$product->key_features = $request->input('key_features');
			$product->instructions = $request->input('instructions');
			$product->slug = str_slug($name);
			// strtolower($request->input('batch_no'));
			$product->batch_no = strtolower($request->input('batch_no'));;
			$product->mfg_date = $request->input('mfg_date');
			$product->expiry_date = $request->input('expiry_date');
			//dimension

			$product->save();
			session()->flash('success','Product Edited');

	    }catch (\Exception $exception){
		    session()->flash('error', 'something went wrong');
	    }

		return redirect()->back();
	}

	public function settings() {
		return view('backend.settings.manage');
	}

	public function postSetting(Request $request  ) {
		try{

			$setting = new setting();
			$setting->name = $request->input('name');
			$setting->value = $request->input('value');
			$setting->save();

			session()->flash('success','Setting Added');
			return redirect()->back();

		}catch (\Exception $exception){
			session()->flash('error','Something went wrong. Please try again.');
		}

		return redirect()->back();
	}

	public function managePurchases() {
		$purchases = sale::where('uid',auth()->user()->uid)->get();

		return view('backend.purchases.manage',[
			'purchases' => $purchases
		]);
	}

	public function manageSales() {
		$sales = sale::all()->sortByDesc('created_at');
		$salesdetail = saleDetail::all()->sortByDesc('created_at');
		$products = product::all()->sortByDesc('created_at');
		$users = User::all()->sortByDesc('created_at');

		return view('backend.sales.manage',[
			'sales' => $sales,
			'salesdetail' => $salesdetail,
			'users' => $users
		]);
	}

	public function deleteSales($sid){
		// $sale = sale::destroy($sid);

		try{
		$sale =	sale::destroy($sid);
			session()->flash('success','Sales Record Deleted Successfully.');

		}catch (\Exception $exception){
			session()->flash('error', 'An error occurred. Please try again.');
		}
		return redirect()->back();


	}

	public function saleDetails( $sid ) {
    	$sale = sale::find($sid);

    	return view('backend.sales.details',[
    		'sale' => $sale
	    ]);
	}

	public function postSales( Request $request ) {
		$sale = new sale();
		$sale->uid = auth()->user()->uid;
		$sale->total = session()->get('carttotal');
		$sale->payment_method = 'cash';
		$sale->save();

		$cartItems = session()->get('cart');
		foreach($cartItems as $item){
			$saleDetail = new saleDetail();
			$saleDetail->sid = $sale->sid;
			$saleDetail->pid = $item->pid;
			$saleDetail->quantity = $item['quantity'];
			$saleDetail->save();
		}
		session()->remove('cart');

		//this is the problem 
		//session()->pull('carttotal', $cartItems);

		
		

		session()->flash('success','Sales Completed Successfully. Please make payment');

		// return redirect('pay/' . $sale->sid);
		return redirect('/shop');
	}

	// the calculate total function
	
	public function getExpiredProducts(){

	$currentDate = date('Y-m-d');
	$products = product::where('expiry_date', '<=', $currentDate)->get();

	// if ($products->expiry_date ) {
		

	//  }

	return view ('backend.products.expired',[
		'products' => $products
	]);


	}


	public function pay($sid) {
    	$sale = sale::find($sid);
		return view('frontend.pay',[
			'sale' => $sale
		]);
	}

	public function users() {
    	$users = User::all();
    	return view('backend.users.manage',[
    		'users' => $users
	    ]);

	}

	public function editUser( $uid ) {
		$user = User::find($uid);
		return view('backend.users.edit',[
			'user' => $user
		]);
	}

	public function postEditUser( $uid ) {
		try{
			$user = User::find($uid);
			$user->update(request()->all());
			session()->flash('success','User Edited');
		}catch (\Exception $exception){
			session()->flash('error','Sorry something went wrong. Please try again');
		}
		return redirect()->back();
	}

	public function manageAdmins() {
		$admins = User::where('role','admin')->get();

		return view('backend.admins.manage',[
			'admins' => $admins
		]);
	}


	public function logout() {
		auth()->logout();
		return redirect('/');
	}

	public function changePassword() {
		return view('changePassword');
	}

	public function postChangePassword( Request $request ) {
		$oldpassword = $request->input('oldpassword');
		$password = $request->input('password');
		$confirmpassword = $request->input('confirmpassword');
		$user = User::find(Auth::user()->uid);

		if(password_verify($oldpassword,$user->password) ){

			if($confirmpassword == $password){
				$user->password = bcrypt($password);
				$request->session()->flash("success","Password changed successfully");
				$user->save();
				return redirect('/change-password');

			}
			else{
				$request->session()->flash("error", "Both new passwords don't match. Please try again");
				return redirect('/change-password');
			}

		} else {
			$request->session()->flash("error", "Current Password is wrong. Please try again.");
			return redirect('/change-password');
		}

	}

}
