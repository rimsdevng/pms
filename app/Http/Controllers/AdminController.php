<?php

namespace App\Http\Controllers;

use App\category;
use App\product;
use App\productImage;
use App\sale;
use App\saleDetail;
use App\setting;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
	    $user_ip = getenv('REMOTE_ADDR');
	    $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
	    $country = $geo["geoplugin_countryName"];
	    session()->put('country',$country);

    }

    public function index()
    {
    	$products = product::all();
    	$users = User::where('role','user')->get();
    	$sales = sale::all();
        return view('backend.dashboard',[
        	'products' => $products,
	        'users'    => $users,
	        'sales'    => $sales
        ]);
    }

	public function profile() {
		return view('frontend.account.profile');
    }

	public function checkout() {
		return view('frontend.checkout');
    }

	public function addCategory() {
		return view('backend.categories.add');
	}

	public function editCategory($catid) {

		$category = category::find($catid);

		return view('backend.categories.edit',[
			'category' => $category
		]);
	}

	public function deleteCategory($catid) {
		try{
			category::destroy($catid);
			session()->flash('success','Category Deleted.');

		}catch (\Exception $exception){
			session()->flash('error', 'An error occurred. Please try again.');
		}
		return redirect()->back();
	}

	public function postAddCategory(Request $request) {

		try{
			$name = $request->input('name');
			$category  = new category();
			$category->slug = str_slug($name);
			$category->name = $name;
			$category->description = $request->input('description');
			$category->save();

			session()->flash('success', 'Category added');

		}catch (\Exception $exception){

			session()->flash('error','Something went Wrong');

		}

		return redirect()->back();

	}

	public function postEditCategory( $catid ) {

    	try{

		    $category =  category::find($catid);

		    $category->update(request()->all());

		    session()->flash('success','Category Edited');

	    }catch (\Exception $exception){
		    session()->flash('error', 'something went wrong');
	    }


		return redirect()->back();
	}

	public function manageProducts() {
    	$products = product::all();
		return view('backend.products.manage',[
			'products' => $products
		]);
	}

	public function addProduct() {
    	$categories = category::all();
		return view('backend.products.add',[
			'categories' => $categories
		]);
	}

	public function editProduct($pid) {
		$product = product::find($pid);

		return view('backend.products.edit',[
			'product' => $product
		]);

	}

	public function deleteProduct($pid) {

    	try{
		    product::destroy($pid);
		    session()->flash('success','Product Deleted');
	    }catch (\Exception $exception){
		    session()->flash('error','something went wrong');
	    }

		return redirect()->back();

	}

	public function postAddProduct(Request $request  ) {

		$name = $request->input('name');

		try{
			$product = new product();
			$product->name = $name;
			$product->catid = $request->input('catid');
			$product->price = $request->input('price');
			$product->key_features = $request->input('key_features');
			$product->manufacturer = $request->input('manufacturer');
			$product->designer = $request->input('designer');
			$product->made_in = $request->input('made_in');
			$product->slug = str_slug($name);
			//dimension

			$product->height = $request->input('height');
			$product->width = $request->input('width');
			$product->length = $request->input('length');
			$product->diameter = $request->input('diameter');
			$product->weight = $request->input('weight');
			$product->cord_length = $request->input('cord_length');
			$product->shade_size  = $request->input('shade_size');

			$product->light_bulb = $request->input('light_bulb');
			$product->material = $request->input('material');

			$product->assembly_instruction = $request->input('assembly_instruction');

			$product->save();

		if($request->hasFile('images')){
			foreach ( $request->file( 'images' ) as $item ) {
				$rand = Carbon::now()->timestamp;
				$inputFileName = $item->getClientOriginalName();

				$item->move( "uploads", $rand . $inputFileName );
				$url = url("/uploads/" . $rand . $inputFileName);
				$productImage = new productImage();
				$productImage->pid = $product->pid;
				$productImage->url = $url;
				$productImage->save();

			}

		}


		session()->flash('success', 'Product Added.');

		}catch (\Exception $exception){
			session()->flash('error', 'An error occurred. Please try again');
		}

		return redirect()->back();
	}

	public function postEditProduct(Request $request, $pid) {

//    	try{

			$name = $request->input('name');
		    $product = product::find($pid);
			$product->name = $name;;
			$product->catid = $request->input('catid');
			$product->price = $request->input('price');
			$product->key_features = $request->input('key_features');
			$product->manufacturer = $request->input('manufacturer');
			$product->designer = $request->input('designer');
			$product->made_in = $request->input('made_in');
			$product->slug = str_slug($name);
			//dimension

			$product->height = $request->input('height');
			$product->width = $request->input('width');
			$product->length = $request->input('length');
			$product->diameter = $request->input('diameter');
			$product->weight = $request->input('weight');
			$product->cord_length = $request->input('cord_length');
			$product->shade_size  = $request->input('shade_size');

			$product->light_bulb = $request->input('light_bulb');
			$product->material = $request->input('material');

			$product->assembly_instruction = $request->input('assembly_instruction');

			$product->save();

			session()->flash('success','Product Edited');

//	    }catch (\Exception $exception){
//		    session()->flash('error', 'something went wrong');
//	    }

		return redirect()->back();
	}

	public function settings() {
		return view('backend.settings.manage');
	}

	public function postSetting(Request $request  ) {
		try{

			$setting = new setting();
			$setting->name = $request->input('name');
			$setting->value = $request->input('value');
			$setting->save();

			session()->flash('success','Setting Added');
			return redirect()->back();

		}catch (\Exception $exception){
			session()->flash('error','Something went wrong. Please try again.');
		}

		return redirect()->back();
	}

	public function managePurchases() {
		$purchases = sale::where('uid',auth()->user()->uid)->get();

		return view('backend.purchases.manage',[
			'purchases' => $purchases
		]);
	}

	public function manageSales() {
		$sales = sale::all()->sortByDesc('created_at');

		return view('backend.sales.manage',[
			'sales' => $sales
		]);
	}

	public function saleDetails( $sid ) {
    	$sale = sale::find($sid);

    	return view('backend.sales.details',[
    		'sale' => $sale
	    ]);
	}

	public function postSales( Request $request ) {
		$sale = new sale();
		$sale->uid = auth()->user()->uid;
		$sale->address = $request->input('address');
		$sale->total = session()->get('carttotal');
		$sale->payment_method = 'card';
		$sale->save();

		$cartItems = session()->get('cart');
		foreach($cartItems as $item){
			$saleDetail = new saleDetail();
			$saleDetail->sid = $sale->sid;
			$saleDetail->pid = $item->pid;
			$saleDetail->quantity = $item['quantity'];
			$saleDetail->save();
		}
		session()->remove('cart');
		session()->flash('success','Order Placed. Please make payment');

		return redirect('pay/' . $sale->sid);
	}

	public function pay($sid) {
    	$sale = sale::find($sid);
		return view('frontend.pay',[
			'sale' => $sale
		]);
	}

	public function users() {
    	$users = User::all();
    	return view('backend.users.manage',[
    		'users' => $users
	    ]);

	}

	public function editUser( $uid ) {
		$user = User::find($uid);
		return view('backend.users.edit',[
			'user' => $user
		]);
	}

	public function postEditUser( $uid ) {
		try{
			$user = User::find($uid);
			$user->update(request()->all());
			session()->flash('success','User Edited');
		}catch (\Exception $exception){
			session()->flash('error','Sorry something went wrong. Please try again');
		}
		return redirect()->back();
	}

	public function manageAdmins() {
		$admins = User::where('role','admin')->get();

		return view('backend.admins.manage',[
			'admins' => $admins
		]);
	}


	public function logout() {
		auth()->logout();
		return redirect('/');
	}


}
