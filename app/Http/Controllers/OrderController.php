<?php

namespace App\Http\Controllers;

use App\category;
use App\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{

	public function __construct() {
//		$user_ip = getenv('REMOTE_ADDR');
//		$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
//		$country = $geo["geoplugin_countryName"];
//		session()->put('country',$country);

		if(!session()->has('carttotal')) session()->put('carttotal',0);
	}
	public function index( Request $request ) {

		$products = product::all();
		$categories = category::all();

		return view('frontend.home',[
			'products' => $products,
			'categories' => $categories
		]);
	}

	public function categorySearch( $slug ) {
//		session()->invalidate();
		$category = category::where('slug',$slug)->first();

		try{
			$products = $category->Products;
		}catch (\Exception $exception){
			$products = [];
		}


		return view('frontend.products.search',[
			'products' => $products
		]);
	}

	public function productDetails($slug) {

		$product  = product::where('slug',$slug)->first();

		return view('frontend.products.details',[
			'product' => $product
		]);


	}

	//returns the page with list of products
	public function shop() {

		// $products = product::paginate(10);

		$currentDate = date('Y-m-d');
		$products = product::where('expiry_date', '>=', $currentDate)->get();

		return view('backend.sales.add',[
			'products' => $products
		]);

	}

	public function addToCart($pid) {


		if(session()->has('cart')){
			$cart = session()->get('cart');

			$cartArray = array();
			foreach (session()->get('cart') as $item) {
				array_push($cartArray, $item->pid);
			}


			if( collect($cart)->where('pid',$pid)->count() <= 0) {
				$product = product::find( $pid );

				if(Input::has('quantity')) $quantity = Input::get('quantity'); else $quantity = 1;

				$product['quantity'] = $quantity;


				session()->push( 'cart', $product );
				//$this->calculateTotal();
				session()->flash( 'success', 'Item added to cart' );
			} else {
				session()->flash( 'error', 'Item is already in your cart' );
			}

		} else{
			$product = product::find($pid);
			if(Input::has('quantity')) $quantity = Input::get('quantity'); else $quantity = 1;

			$product['quantity'] = $quantity;
			session()->push('cart',$product);
			//$this->calculateTotal();
			session()->flash('success','Item added to cart');
		}

		$this->calculateTotal();

		return redirect()->back();

	}

	public function calculateTotal() {
		$total = 0;

		foreach (session()->get('cart') as $item){
			$total += $item->price * $item['quantity'];
		}

		session()->put('carttotal',$total);
	}
	public function removeFromCart($pid) {


		if(session()->has('cart')){
			$cart = session()->get('cart');
			$product = product::find($pid);

			$newCart = array();
			foreach($cart as $item){
				if($item->pid != $product->pid){
					array_push($newCart,$item);
				}
			}

			session()->put('cart',$newCart);
			session()->flash('success','Item removed from cart');

		} else{
			session()->flash('error','Item is not in your cart');
		}

		$this->calculateTotal();
		return redirect()->back();

	}

	// public function getTestSales(){
	// 	// variable $slug was parsed in as a parameter into the function earlier
	// 	// $category = category::where('slug',$slug)->first();

	// 	try{
	// 		$products = $category->Products;
	// 	}catch (\Exception $exception){
	// 		$products = [];
	// 	}


	// 	return view('backend.sales.add',[
	// 		'products' => $products
	// 	]);
	// 	// return view ('backend.sales.add');

	// }

}
