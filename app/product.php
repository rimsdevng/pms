<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //
    protected  $primaryKey = 'pid';
    public  $table = 'products';
    protected $guarded = [ ];

	public function Images() {
		return $this->hasMany(productImage::class,'pid','pid');
    }

    // new added below
    public function saleDetail() {
      return $this->hasMany(saleDetail::class,'pid','pid');
    }
  

	public function Category() {
		return $this->belongsTo(category::class,'catid','catid');
    }
}
