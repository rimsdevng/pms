<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected  $primaryKey = 'catid';
    public  $table = 'categories';
    protected $guarded = [ ];

    public function Products() {
		return $this->hasMany(product::class,'catid','catid');
    }


}
