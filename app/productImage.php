<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productImage extends Model
{
    //
    protected $primaryKey = 'pimid';
    protected $table = 'product_images';
    protected $guarded = [ ];


	public function Product() {
		return $this->belongsTo(product::class,'pid','pid');
    }
}
