<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table){
            $table->increments('pid');
            // $table->integer('uid');
		    $table->string('slug',191)->uinque();
		    $table->integer('catid');
		    $table->integer('price'); // only cedi's prices would be stored
		    $table->string('name');

		    $table->string('manufacturer')->nullable();
		    
		    $table->string('made_in')->nullable();

		    $table->string('components')->nullable();
		    $table->string('key_features',4000)->nullable();
		    $table->string('instructions')->nullable();
		    $table->timestamps();

	    });

        Schema::create('product_images',function (Blueprint $table){
		    $table->increments('piid');
		    $table->integer('pid');
            $table->string('url');
          //  $table->integer('uid');
		    $table->timestamps();
        });

	    Schema::create('settings',function (Blueprint $table){
		    $table->increments('setid');
		    $table->string('name');
		    $table->string('value');
		    $table->timestamps();
	    });

	    Schema::create('categories',function (Blueprint $table){
		    $table->increments('catid');
		    $table->string('name');
		    $table->string('description');
		    $table->timestamps();
	    });

	 

	    Schema::create('sales',function (Blueprint $table){
		    $table->increments('sid');
		    $table->integer('uid');
		    $table->string('total');
		    $table->string('payment_method');
		    $table->timestamps();
	    });

	    // Schema::create('order_details',function (Blueprint $table){
		//     $table->increments('odid');
		//     $table->integer('oid');
		//     $table->integer('mpid');
		//     $table->integer('quantity');
		//     $table->timestamps();
	    // });

        Schema::create('sales_histories',function (Blueprint $table){
		    $table->increments('shid');
		    $table->integer('sid');
		    $table->integer('pid');
		    $table->integer('quantity');
		    $table->timestamps();
	    });

	    Schema::create('payments',function (Blueprint $table){
		    $table->increments('payid');
		    $table->integer('sid');
		    $table->string('amount');
		    $table->string('reference');
		    $table->string('method')->default('paystack');
		    $table->string('others')->nullable();
		    $table->timestamps();
	    });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_images'); 
        Schema::dropIfExists('settings');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('sales');
        Schema::dropIfExists('sales_histories');
        Schema::dropIfExists('payments');
    }
}
