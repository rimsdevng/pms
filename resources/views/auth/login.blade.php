@extends('layouts.auth')

@section('content')


<div class="container">
        <div class="row justify-content-center">
            <div class="col-4">
                  <div class="misc-header text-center">
                <a href="{{ url('/') }}">
                    <img alt="" src="backend/assets/img/icon.png" class="logo-icon margin-r-10">
                    <img alt="" src="backend/assets/img/logo-dark.png" class="toggle-none hidden-xs">
                </a>
                </div>
                <div class="misc-box">   
                        <form role="form" method="POST" action="{{ route('login') }}">
                                @csrf                        <div class="form-group">                                      
                            <label  for="exampleuser1">Email</label>
                            <div class="group-icon">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            <span class="icon-user text-muted icon-input"></span>
                            </div>
                            
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <div class="group-icon">
                            
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                <span class="icon-lock text-muted icon-input"></span>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            
                        </div>
                        <div class="clearfix">
                            <div class="float-left">
                               <div class="checkbox checkbox-primary margin-r-5">
                                    
                                   <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                   <label for="checkbox2"> Remember Me </label>
                                </div>
                            </div>
                            <div class="float-right">
                                <button type="submit" class="btn btn-block btn-primary btn-rounded box-shadow">Login</button>
                            </div>
                        </div>
                        <hr>

                        <p class="text-center">Forgotten Password?</p>
                        <a class="btn btn-block btn-success btn-rounded box-shadow" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                    </form>
                </div>
                <div class="text-center misc-footer">
                   <p>Copyright &copy; <?php echo date('Y')?> GhanaPlus</p>
                </div>
            </div>
        </div>
    </div>







{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
