@extends('layouts.backend')

@section('content')
    <section class="section-container">

        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-heading">
                <div>Dashboard
                    <small data-localize="dashboard.WELCOME"></small>
                </div>
            </div>
            <!-- START cards box-->
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <!-- START card-->
                    <div class="card flex-row align-items-center align-items-stretch border-0">
                        <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left">
                            <em class="fa fa-archive fa-3x"></em>
                        </div>
                        <div class="col-8 py-3 bg-primary rounded-right">
                            <div class="h2 mt-0">{{count($products)}}</div>
                            <div class="text-uppercase">Products</div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <!-- START card-->
                    <div class="card flex-row align-items-center align-items-stretch border-0">
                        <div class="col-4 d-flex align-items-center bg-green-dark justify-content-center rounded-left">
                            <em class="fa-3x" style="font-style: normal">&#x20A6;</em>
                        </div>
                        <div class="col-8 py-3 bg-green rounded-right">
                            <div class="h2 mt-0">{{count($sales)}}
                            </div>
                            <div class="text-uppercase">Sales</div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-12">
                    <!-- START card-->
                    <div class="card flex-row align-items-center align-items-stretch border-0">
                        <div class="col-4 d-flex align-items-center bg-purple-dark justify-content-center rounded-left">
                            <em class="fa fa-users fa-3x"></em>
                        </div>
                        <div class="col-8 py-3 bg-purple rounded-right">
                            <div class="h2 mt-0">{{count($users)}}</div>
                            <div class="text-uppercase">Users</div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-12">
                    <!-- START date widget-->
                    <div class="card flex-row align-items-center align-items-stretch border-0">
                        <div class="col-4 d-flex align-items-center bg-green justify-content-center rounded-left">
                            <div class="text-center">
                                <!-- See formats: https://docs.angularjs.org/api/ng/filter/date-->
                                <div class="text-sm" data-now="" data-format="MMMM"></div>
                                <br>
                                <div class="h2 mt-0" data-now="" data-format="D"></div>
                            </div>
                        </div>
                        <div class="col-8 py-3 rounded-right">
                            <div class="text-uppercase" data-now="" data-format="dddd"></div>
                            <br>
                            <div class="h2 mt-0" data-now="" data-format="h:mm"></div>
                            <div class="text-muted text-sm" data-now="" data-format="a"></div>
                        </div>
                    </div>
                    <!-- END date widget-->
                </div>
            </div>
            <!-- END cards box-->
            <div class="row">
                <!-- START dashboard sidebar-->

                <aside class="col-md-6">
                    <!-- START messages and activity-->
                    <div class="card card-default">
                        <div class="card-header">
                            <div class="card-title">Latest Users</div>
                        </div>
                        <!-- START list group-->
                        <div class="list-group">
                            <!-- START list group item-->

                            @if(isset($users[0]))
                            <div class="list-group-item">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                 <span class="fa-stack">
                                    <em class="fa fa-circle fa-stack-2x text-purple"></em>
                                    <em class="fa fa-archive fa-stack-1x fa-inverse text-white"></em>
                                 </span>
                                    </div>
                                    <div class="media-body text-truncate">
                                        <p class="mb-1"><a class="text-purple m-0" href="#">User #{{$users[0]->uid}}</a>
                                        </p>
                                        <p class="m-0">
                                            <small><a href="{{url('manage-users/'. $users[0]->uid)}}">{{$users[0]->fname}} {{$users[0]->sname}} </a>
                                            </small>
                                        </p>
                                    </div>
                                    <div class="ml-auto">
                                        <small class="text-muted ml-2">{{$users[0]->created_at->diffForHumans()}}</small>
                                    </div>
                                </div>
                            </div>
                            @endif

                        </div>
                        <!-- END list group-->
                        <!-- START card footer-->
                        <div class="card-footer"><a class="text-sm" href="{{url('manage-users')}}">All Users</a>
                        </div>
                        <!-- END card-footer-->
                    </div>
                    <!-- END messages and activity-->
                </aside>
                <aside class="col-md-6">
                    <!-- START messages and activity-->
                    <div class="card card-default">
                        <div class="card-header">
                            <div class="card-title">Latest Products</div>
                        </div>
                        <!-- START list group-->
                        <div class="list-group">
                            <!-- START list group item-->

                            @if(isset($products[0]))
                            <div class="list-group-item">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                 <span class="fa-stack">
                                    <em class="fa fa-circle fa-stack-2x text-purple"></em>
                                    <em class="fa fa-archive fa-stack-1x fa-inverse text-white"></em>
                                 </span>
                                    </div>
                                    <div class="media-body text-truncate">
                                        <p class="mb-1"><a class="text-purple m-0" href="#">Product #{{$products[0]->pid}}</a>
                                        </p>
                                        <p class="m-0">
                                            <small><a href="{{url('product/'. $products[0]->pid)}}">{{$products[0]->name}} </a>
                                            </small>
                                        </p>
                                    </div>
                                    <div class="ml-auto">
                                        <small class="text-muted ml-2">{{$products[0]->created_at->diffForHumans()}}</small>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <!-- END list group item-->

                        {{--@if(isset($products[1]))--}}
                            {{--<!-- START list group item-->--}}
                            {{--<div class="list-group-item">--}}
                                {{--<div class="media">--}}
                                    {{--<div class="align-self-start mr-2">--}}
                                 {{--<span class="fa-stack">--}}
                                    {{--<em class="fa fa-circle fa-stack-2x text-info"></em>--}}
                                    {{--<em class="fa fa-file-text-o fa-stack-1x fa-inverse text-white"></em>--}}
                                 {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-truncate">--}}
                                        {{--<p class="mb-1"><a class="text-info m-0" href="#">NEW DOCUMENT</a>--}}
                                        {{--</p>--}}
                                        {{--<p class="m-0">--}}
                                            {{--<small><a href="#">Bootstrap.doc</a>--}}
                                            {{--</small>--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="ml-auto">--}}
                                        {{--<small class="text-muted ml-2">{{$products[0]->created_at->diffForHumans()}}</small>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- END list group item-->--}}
                            {{--@endif--}}


                        {{--@if(isset($products[2]))--}}
                            {{--<!-- START list group item-->--}}
                            {{--<div class="list-group-item">--}}
                                {{--<div class="media">--}}
                                    {{--<div class="align-self-start mr-2">--}}
                                 {{--<span class="fa-stack">--}}
                                    {{--<em class="fa fa-circle fa-stack-2x text-danger"></em>--}}
                                    {{--<em class="fa fa-exclamation fa-stack-1x fa-inverse text-white"></em>--}}
                                 {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-truncate">--}}
                                        {{--<p class="mb-1"><a class="text-danger m-0" href="#">BROADCAST</a>--}}
                                        {{--</p>--}}
                                        {{--<p class="m-0"><a href="#">Read</a>--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="ml-auto">--}}
                                        {{--<small class="text-muted ml-2">5h</small>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- END list group item-->--}}

                            {{--@endif--}}


                        {{--@if(isset($products[0]))--}}
                            {{--<!-- START list group item-->--}}
                            {{--<div class="list-group-item">--}}
                                {{--<div class="media">--}}
                                    {{--<div class="align-self-start mr-2">--}}
                                 {{--<span class="fa-stack">--}}
                                    {{--<em class="fa fa-circle fa-stack-2x text-success"></em>--}}
                                    {{--<em class="fa fa-clock-o fa-stack-1x fa-inverse text-white"></em>--}}
                                 {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-truncate">--}}
                                        {{--<p class="mb-1"><a class="text-success m-0" href="#">NEW MEETING</a>--}}
                                        {{--</p>--}}
                                        {{--<p class="m-0">--}}
                                            {{--<small>On--}}
                                                {{--<em>10/12/2015 09:00 am</em>--}}
                                            {{--</small>--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="ml-auto">--}}
                                        {{--<small class="text-muted ml-2">15h</small>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- END list group item-->--}}
                            {{--@endif--}}

                        {{--@if(isset($products[0]))--}}
                            {{--<!-- START list group item-->--}}
                            {{--<div class="list-group-item">--}}
                                {{--<div class="media">--}}
                                    {{--<div class="align-self-start mr-2">--}}
                                 {{--<span class="fa-stack">--}}
                                    {{--<em class="fa fa-circle fa-stack-2x text-warning"></em>--}}
                                    {{--<em class="fa fa-tasks fa-stack-1x fa-inverse text-white"></em>--}}
                                 {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body text-truncate">--}}
                                        {{--<p class="mb-1"><a class="text-warning m-0" href="#">TASKS COMPLETION</a>--}}
                                        {{--</p>--}}
                                        {{--<div class="progress progress-xs m-0">--}}
                                            {{--<div class="progress-bar bg-warning progress-bar-striped" role="progressbar" aria-valuenow="22" aria-valuemin="0" aria-valuemax="100" style="width: 22%;">--}}
                                                {{--<span class="sr-only">22% Complete</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="ml-auto">--}}
                                        {{--<small class="text-muted ml-2">1w</small>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- END list group item-->--}}
                            {{--@endif--}}
                        {{--</div>--}}
                        <!-- END list group-->
                        <!-- START card footer-->
                        <div class="card-footer"><a class="text-sm" href="{{url('manage-products')}}">All Products</a>
                        </div>
                        <!-- END card-footer-->
                    </div>
                    <!-- END messages and activity-->
                </aside>
                <!-- END dashboard sidebar-->
            </div>
        </div>
    </section>


@endsection