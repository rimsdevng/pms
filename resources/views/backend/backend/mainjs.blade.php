<script>
    $(document).ready(function () {
        var form = $("#product-form");
        form.validate({
            errorPlacement: function (e, t) {
                t.before(e)
            }, rules: {confirm: {equalTo: "#password"}}
        }), form.children("div").steps({
            headerTag: "h4",
            bodyTag: "fieldset",
            transitionEffect: "slideLeft",
            onStepChanging: function (t, o, a) {
                return form.validate().settings.ignore = ":disabled,:hidden", form.valid()
            },
            onFinishing: function (t, o) {
                return form.validate().settings.ignore = ":disabled", form.valid()
            },
            onFinished: function (e, t) {
                form.submit();
            }
        });

        $('a').tooltip();
    });

</script>
