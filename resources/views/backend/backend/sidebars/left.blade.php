<aside class="aside-container">
    <!-- START Sidebar (left)-->
    <div class="aside-inner" style="margin-left: 0">
        <nav class="sidebar" data-sidebar-anyclick-close="">
            <!-- START sidebar nav-->
            <ul class="sidebar-nav">
                <!-- START user info-->
                <li class="has-user-block">
                    <div class="collapse" id="user-block">
                        <div class="item user-block">
                            <!-- User picture-->
                            <div class="user-block-picture">
                                <div class="user-block-status">
                                    {{--<img class="img-thumbnail rounded-circle" src="{{url('')}}" alt="Avatar" width="60" height="60">--}}
                                    <div class="circle bg-success circle-lg"></div>
                                </div>
                            </div>
                            <!-- Name and Job-->
                            <div class="user-block-info">
                                <span class="user-block-name">Hello, {{auth()->user()->fname}} {{auth()->user()->sname}}</span>
                                <span class="user-block-role">{{auth()->user()->role}}</span>
                            </div>
                        </div>
                    </div>
                </li>
                <!-- END user info-->
                <!-- Iterates over all sidebar items-->
                <li class="nav-heading ">
                    <span data-localize="sidebar.heading.HEADER">Main Navigation</span>
                </li>
                <li class=" ">
                    <a href="{{url('dashboard')}}" >
                        {{--<div class="float-right badge badge-success">30</div>--}}
                        <em class="icon-speedometer"></em>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class=" ">
                    <a href="{{url('purchases')}}" >
                        <em class="icon-layers"></em>
                        <span>Purchases</span>
                    </a>
                </li>


                @if(auth()->user()->role == 'admin')
                <li class="nav-heading ">
                    <span>ADMIN</span>
                </li>

                <li class=" ">
                    <a href="#sales" data-toggle="collapse">
                        <em class="icon-layers"></em>
                        <span>Sales</span>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="sales">
                        {{--<li class="sidebar-subnav-header">Add</li>--}}
                        <li class=" ">
                            <a href="{{url('add-sale')}}" >
                                <span>Add</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{url('manage-sales')}}">
                                <span>Manage</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class=" ">

                    <a href="#products" data-toggle="collapse">

                        {{--<div class="float-right badge badge-success">3</div>--}}
                        <em class="fa fa-archive"></em>

                        <span>Products</span> <i  class="fa fa-caret-right" style="margin-left: 10px;" ></i>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="products">
                        {{--<li class="sidebar-subnav-header">Add</li>--}}
                        <li class=" active">
                            <a href="{{url('add-product')}}" >
                                <span>Add</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{url('manage-products')}}" >
                                <span>Manage</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class=" ">
                    <a href="#users"  data-toggle="collapse">
                        <em class="fa fa-users"></em>
                        <span>Users</span>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="users">
                        {{--<li class="sidebar-subnav-header">Add</li>--}}
                        <li class=" ">
                            <a href="{{url('add-user')}}" >
                                <span>Add</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{url('manage-users')}}" >
                                <span>Manage</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class=" ">
                    <a href="#admins"  data-toggle="collapse">
                        <em class="fa fa-user-secret"></em>
                        <span>Admins</span>
                    </a>
                    <ul class="sidebar-nav sidebar-subnav collapse" id="admins">
                        {{--<li class="sidebar-subnav-header">Add</li>--}}
                        <li class=" ">
                            <a href="{{url('add-admin')}}" >
                                <span>Add</span>
                            </a>
                        </li>
                        <li class=" ">
                            <a href="{{url('manage-admins')}}" >
                                <span>Manage</span>
                            </a>
                        </li>
                    </ul>
                </li>
                {{--<li class=" ">--}}
                    {{--<a href="#invoices"  data-toggle="collapse">--}}
                        {{--<em class="fa fa-file-text"></em>--}}
                        {{--<span>Invoices</span>--}}
                    {{--</a>--}}
                    {{--<ul class="sidebar-nav sidebar-subnav collapse" id="invoices">--}}
                        {{--<li class="sidebar-subnav-header">Add</li>--}}
                        {{--<li class=" ">--}}
                            {{--<a href="{{url('add-invoice')}}" >--}}
                                {{--<span>Add</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class=" ">--}}
                            {{--<a href="{{url('manage-invoices')}}">--}}
                                {{--<span>Manage</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class=" ">--}}
                    {{--<a href="#receipts" data-toggle="collapse">--}}
                        {{--<em class="fa fa-file-text-o"></em>--}}
                        {{--<span>Receipts</span>--}}
                    {{--</a>--}}
                    {{--<ul class="sidebar-nav sidebar-subnav collapse" id="receipts">--}}
                        {{--<li class="sidebar-subnav-header">Add</li>--}}
                        {{--<li class=" ">--}}
                            {{--<a href="{{url('add-receipt')}}">--}}
                                {{--<span>Add</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class=" ">--}}
                            {{--<a href="{{url('manage-receipts')}}" >--}}
                                {{--<span>Manage</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                <li class=" ">
                    <a href="{{url('settings')}}" >
                        <em class="fa fa-cog"></em>
                        <span>Settings</span>
                    </a>
                </li>

                @endif
            </ul>
            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>