@extends('layouts.backend')

@section('content')

    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            @include('notification')
            <div class="content-heading">
                <a href="{{url('manage-sales')}}" style="margin-right: 10px;">
                    <i class="fa fa-arrow-left"></i>
                </a>

                <div>Sale  #{{$sale->sid}}
                    {{--<small>Manage site Setting</small>--}}
                </div>
            </div>


            <div class="card card-default">
                <div class="card-header">Sale #{{$sale->sid}} Details</div>
                <div class="card-body">

                    <span>Customer : {{$sale->User->fname}} {{$sale->User->sname}}</span><br>
                    <span>Total: &#x20A6;{{$sale->total}}</span><br>
                    <span>{{count($sale->Details)}} product(s)</span><br>
                    <span>Date: {{$sale->created_at->toDayDateTimeString()}}</span><br>
                    <span>Payment Status : <label class="label label-success">Paid</label> </span><br>

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Product ID</th>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($sale->Details as $detail)
                            <tr>
                                <td>{{$detail->pid}}</td>
                                <td>{{$detail->Product->name}}</td>
                                <td>{{$detail->quantity}}</td>
                                <td>
                                    <a href="{{url('product/' . $detail->Product->slug)}}" title="View" class="btn btn-primary">
                                        <i class="fa fa-eye"></i>
                                    </a>

                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                    <a class="btn text-white btn-primary" >Download Invoice</a>
                    <a class="btn btn-primary text-white" >Download Receipt</a>
                </div>
            </div>

        </div>
    </section>


@endsection