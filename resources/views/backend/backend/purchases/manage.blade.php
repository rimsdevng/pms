@extends('layouts.backend')

@section('content')

    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            @include('notification')
            <div class="content-heading">
                <div>Sales
                    <small>Manage sales</small>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <table class="table table-striped my-4 w-100" id="datatable1">
                        <thead>
                        <tr>
                            <th>Sale ID</th>
                            <th>No Of Products</th>
                            <th style="width: 100px">Total (&#x20A6;)</th>
                            <th>Date Sold</th>
                            <th style="width: 150px;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($purchases as $sale)
                            <tr>
                                <td>{{$sale->sid}}</td>
                                <td>{{count($sale->Details)}}</td>
                                <td>{{$sale->total}}</td>
                                <td>{{$sale->created_at->toDayDateTimeString()}}</td>
                                <td>
                                    <a href="{{url('sale/' . $sale->sid)}}" title="View" class="btn btn-primary">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    {{--<a href="{{url('edit-sale/' . $sale->sid)}}" title="Edit" class="btn btn-warning">--}}
                                    {{--<i class="fa fa-edit"></i>--}}
                                    {{--</a>--}}
                                    {{--<a href="{{url('delete-sale/' . $sale->sid)}}" title="Delete" class="btn btn-danger">--}}
                                    {{--<i class="fa fa-trash"></i>--}}
                                    {{--</a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </section>
@endsection