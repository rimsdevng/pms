@extends('layouts.backend')

@section('content')

    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            @include('notification')
            <div class="content-heading">
                <div>Admins
                    <small>Manage admins</small>
                </div>
            </div>

            <div class="card">

                <div class="card-body">
                    <table class="table table-striped my-4 w-100" id="datatable1">
                        <thead>
                        <tr>
                            <th data-priority="1">Admin ID</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th class="sort-alpha" data-priority="2">Date Registered</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($admins as $admin)
                        <tr class="gradeX">
                            <td>{{$admin->uid}}</td>
                            <td>{{$admin->fname}} {{$admin->sname}}</td>
                            <td>{{$admin->phone}}</td>
                            <td>{{$admin->created_at->toDayDateTimeString()}}</td>
                            <td>
                                {{--<a href="{{url('user/' . $admin->uid)}}" class="btn btn-primary">--}}
                                    {{--<i class="fa fa-eye"></i>--}}
                                {{--</a>--}}
                                <a href="{{url('edit-user/' . $admin->uid)}}"  title="Edit" class="btn btn-warning">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{url('delete-user/' . $admin->uid)}}"  title="Delete" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>

                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </section>
@endsection