@extends('layouts.backend')

@section('content')

    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-heading">
                <div>Users
                    <small>Manage registered users</small>
                </div>
            </div>

            <div class="card">
                {{--<div class="card-header">--}}
                {{--<div class="card-title">Zero configuration</div>--}}
                {{--<div class="text-sm">DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: $().DataTable();.</div>--}}
                {{--</div>--}}
                <div class="card-body">
                    <table class="table table-striped my-4 w-100" id="datatable1">
                        <thead>
                        <tr>
                            <th data-priority="1">User ID</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th class="sort-numeric">No of Purchases</th>
                            <th class="sort-alpha" data-priority="2">Date Registered</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($users as $user)
                        <tr class="gradeX">
                            <td>{{$user->uid}}</td>
                            <td>{{$user->fname}} {{$user->sname}}</td>
                            <td>{{$user->phone}}</td>
                            <td>{{count($user->Sales)}}</td>
                            <td>{{$user->created_at->toDayDateTimeString()}}</td>
                            <td>
                                {{--<a href="{{url('user/' . $user->uid)}}" class="btn btn-primary">--}}
                                    {{--<i class="fa fa-eye"></i>--}}
                                {{--</a>--}}
                                <a href="{{url('edit-user/' . $user->uid)}}" title="Edit" class="btn btn-warning">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{url('delete-user/' . $user->uid)}}" title="Delete" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>

                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </section>
@endsection