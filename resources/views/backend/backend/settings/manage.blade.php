@extends('layouts.backend')

@section('content')

    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            @include('notification')
            <div class="content-heading">
                <div>Settings
                    <small>Manage site Setting</small>
                </div>
            </div>
            <div class="content-heading">
                <div>Current Rate - <span style="color: darkgreen">&#x20A6;{{$rate}}</span>
                </div>
            </div>


            <div class="card card-default">
            <div class="card-header">Edit Exchange Rate</div>
            <div class="card-body">
                <form role="form" method="post" action="{{url('settings')}}">
                    @csrf
                    <input name="name" value="rate" type="hidden">
                    <div class="form-group">
                        <label>Naira rate to one USD</label>
                        <input class="form-control" type="number" name="value" placeholder="360">
                    </div>
                    <button class="btn btn-sm btn-secondary" type="submit">Submit</button>
                </form>
            </div>
        </div>

        </div>
    </section>


@endsection