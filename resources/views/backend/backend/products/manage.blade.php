@extends('layouts.backend')

@section('content')

    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            @include('notification')
            <div class="content-heading">
                <div>Products
                    <small>Manage products</small>
                </div>
            </div>

        <div class="card">
        {{--<div class="card-header">--}}
            {{--<div class="card-title">Zero configuration</div>--}}
            {{--<div class="text-sm">DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: $().DataTable();.</div>--}}
        {{--</div>--}}
        <div class="card-body">
            <table class="table table-striped my-4 w-100" id="datatable1">
                <thead>
                <tr>
                    <th>ID</th>
                    <th data-priority="1">Name</th>
                    <th style="width: 100px">Price (&#x20A6;)</th>
                    <th>Manufacturer</th>
                    <th>Designer</th>
                    <th>Made In</th>
                    <th>Date Added</th>
                    <th style="width: 150px;"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                <tr>
                    <td>{{$product->pid}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->manufacturer}}</td>
                    <td>{{$product->designer}}</td>
                    <td>{{$product->made_in}}</td>
                    <td>{{$product->created_at->toDayDateTimeString()}}</td>
                    <td>
                        <a href="{{url('product/' . $product->slug)}}" title="View" target="_blank" class="btn btn-primary">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a href="{{url('edit-product/' . $product->pid)}}" title="Edit" class="btn btn-warning">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="{{url('delete-product/' . $product->pid)}}" title="Delete" class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

        </div>
    </section>
@endsection