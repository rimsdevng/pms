@extends('layouts.backend')

@section('content')

    <style>
        .wizard > .content{
            min-height: 500px;
        }
    </style>
    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            @include('notification')
            <div class="content-heading">
                <div>Products
                    <small>Add a product</small>
                </div>
            </div>

            <div class="card card-default">
        <div class="card-header">Fill in all details</div>
        <div class="card-body">
            <form id="product-form" enctype="multipart/form-data" method="post" action="{{url('add-product')}}">
                @csrf
                <div>
                    <h4>Product Info
                        <br>
                        <small>Basic information about the product</small>
                    </h4>
                    <fieldset>
                        <label for="userName">Name *</label>
                        <input class="form-control required" id="name" name="name" type="text">
                        <label for="password">Manufacturer *</label>
                        <input class="form-control required" id="manufacturer" name="manufacturer" type="text">
                        <label for="confirm">Designer *</label>
                        <input class="form-control required" id="designer" name="designer" type="text">
                        <label for="confirm">Made In *</label>
                        <input class="form-control required" id="mandein" name="made_in" type="text">

                        <label for="keyfeatures">Key Features *</label>
                        <textarea rows="7" class="form-control required" id="keyfeatures" name="key_features" type="text"></textarea>
                        <p>(*) Mandatory</p>
                    </fieldset>
                    <h4>Additional Info
                        <br>
                        <small>More Descriptive Information</small>
                    </h4>
                    <fieldset>
                        <label for="name">Category *</label>

                        <select class="form-control required" name="catid">
                            @foreach($categories as $category)
                                <option value="{{$category->catid}}">{{$category->name}}</option>
                            @endforeach
                        </select>

                        <label for="price">Price (&#x20A6;) *</label>
                        <input class="form-control required" id="price" name="price" type="text">
                        <label for="height">Height *</label>
                        <input class="form-control required" id="height" name="height" type="text">
                        <label for="email">Width *</label>
                        <input class="form-control required" id="width" name="width" type="text">
                        <label for="length">Length *</label>
                        <input class="form-control required" id="length" name="length" type="text">
                        <label for="diameter">Diameter *</label>
                        <input class="form-control" id="diameter" name="diameter" type="text">
                        <label for="weight">Weight *</label>
                        <input class="form-control" id="weight" name="weight" type="text">
                        <label for="shadesize">Shade Size *</label>
                        <input class="form-control" id="shadesize" name="shade_size" type="text">

                        <p>(*) Mandatory</p>
                    </fieldset>
                    <h4>Finish
                        <br>
                        <small>Extra product information</small>
                    </h4>
                    <fieldset>

                        <label for="cordlength">Cord Length *</label>
                        <input class="form-control required" id="cordlength" name="cord_length" type="text">
                        <label for="lightbulb">Light Bulb *</label>
                        <input class="form-control required" id="lightbulb" name="light_bulb" type="text">
                        <label for="material">Material *</label>
                        <input class="form-control" id="material" name="material" type="text">
                        <label for="assembly">Assembly Instruction *</label>
                        <input class="form-control" id="assemblyinstruction" name="assembly_instruction" type="text">

                    </fieldset>
                    <h4>Product Images
                        <br>
                        <small>Add multiple images</small>
                    </h4>
                    <fieldset>
                        <label>Images</label>
                        <input type="file" name="images[]">
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
        </div>
    </section>


@endsection