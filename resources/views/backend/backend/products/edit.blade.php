@extends('layouts.backend')

@section('content')

    <style>
        .wizard > .content{
            min-height: 500px;
        }
    </style>
    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            @include('notification')
            <div class="content-heading">
                <a href="{{url('manage-products')}}" style="margin-right: 10px;">
                    <i class="fa fa-arrow-left"></i>
                </a>

                <div>Products
                    <small>Edit a product</small>
                </div>
            </div>

            <div class="card card-default">
                <div class="card-header">Fill in all details</div>
                <div class="card-body">
                    <form id="product-form" enctype="multipart/form-data" method="post" action="{{url('edit-product/' . $product->pid)}}">
                        @csrf
                        <div>
                            <h4>Product Info
                                <br>
                                <small>Basic information about the product</small>
                            </h4>
                            <fieldset>
                                <label for="userName">Name *</label>
                                <input class="form-control required" id="name" name="name" value="{{$product->name}}" type="text">
                                <label for="password">Manufacturer *</label>
                                <input class="form-control required" id="manufacturer" name="manufacturer" value="{{$product->manufacturer}}" type="text">
                                <label for="confirm">Designer *</label>
                                <input class="form-control required" id="designer" name="designer" value="{{$product->designer}}" type="text">
                                <label for="confirm">Made In *</label>
                                <input class="form-control required" id="mandein" name="made_in" value="{{$product->made_in}}" type="text">

                                <label for="keyfeatures">Key Features *</label>
                                <textarea rows="7" class="form-control required" id="keyfeatures" name="key_features" type="text">{{$product->key_features}}</textarea>
                                <p>(*) Mandatory</p>
                            </fieldset>
                            <h4>Additional Info
                                <br>
                                <small>More Descriptive Information</small>
                            </h4>
                            <fieldset>
                                <label for="name">Category *</label>

                                <select class="form-control required" name="catid">
                                    @foreach($categories as $category)
                                        <option
                                                @if($category->catid == $product->catid)
                                                        selected
                                                @endif
                                                value="{{$category->catid}}">{{$category->name}}</option>
                                    @endforeach
                                </select>

                                <label for="price">Price (&#x20A6;) *</label>
                                <input class="form-control required" id="price" name="price" value="{{$product->price}}" type="text">
                                <label for="height">Height *</label>
                                <input class="form-control required" id="height" name="height" value="{{$product->height}}" type="text">
                                <label for="email">Width *</label>
                                <input class="form-control required" id="width" name="width" value="{{$product->width}}" type="text">
                                <label for="length">Length *</label>
                                <input class="form-control required" id="length" name="length" value="{{$product->length}}" type="text">
                                <label for="diameter">Diameter *</label>
                                <input class="form-control" id="diameter" name="diameter" value="{{$product->diameter}}" type="text">
                                <label for="weight">Weight *</label>
                                <input class="form-control" id="weight" name="weight" value="{{$product->weight}}" type="text">
                                <label for="shadesize">Shade Size *</label>
                                <input class="form-control" id="shadesize" name="shade_size" value="{{$product->shade_size}}" type="text">

                                <p>(*) Mandatory</p>
                            </fieldset>
                            <h4>Finish
                                <br>
                                <small>Extra product information</small>
                            </h4>
                            <fieldset>

                                <label for="cordlength">Cord Length *</label>
                                <input class="form-control required" id="cordlength" name="cord_length" value="{{$product->cord_length}}" type="text">
                                <label for="lightbulb">Light Bulb *</label>
                                <input class="form-control required" id="lightbulb" name="light_bulb" value="{{$product->light_bulb}}" type="text">
                                <label for="material">Material *</label>
                                <input class="form-control" id="material" name="material" value="{{$product->material}}" type="text">
                                <label for="assembly">Assembly Instruction *</label>
                                <input class="form-control" id="assemblyinstruction" name="assembly_instruction" value="{{$product->assembly_instruction}}" type="text">

                            </fieldset>
                            <h4>Product Images
                                <br>
                                <small>Add multiple images</small>
                            </h4>
                            <fieldset>
                                <label>Images</label>
                                <input type="file" name="images[]">
                            </fieldset>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection