@extends('layouts.sub')

@section('content')




        <!-- ============================================================== -->
		<!-- 						Content Start	 						-->
		<!-- ============================================================== -->
					
            <div class="row page-header"><div class="col-lg-6 align-self-center ">
			  <h2>Products</h2>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Categories</a></li>
					<li class="breadcrumb-item active">Manage</li>
				</ol></div></div>

        <section class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                            @include('notification')
					<div class="card-header card-default">
							Products
							<p class="text-muted">list of products Categories</p>
                        </div>
                        <div class="card-body">
                      
                                <table id="datatable2" class="table table-striped dt-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th style="width:80px">
                                                <strong>S/N</strong>
                                            </th>
                                            <th>
                                                <strong>CATEGORY NAME</strong>
                                            </th>
                                            <th>
                                                <strong>DESCRIPTION</strong>
                                            </th>
                                            
                                            <th class="text-center">
                                                <strong>ACTION</strong>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                @if(count($categories) == 0)
                                    <div>No Products available</div>
                                @endif
                                    @if(count($categories) > 0)
                                        <?php $count = 1; ?>

                                        @foreach($categories as $category)
                                            
                                        <tr>
                                            <td>       
                                                    #<?php echo $count;?>.
                                            </td>
                                            <td>{{ $category->name }}</td>


                                            <td>{{ $category->description }}</td>
                                            
                                            <td class="text-center">
                                            
                                                <button type="button" class="btn btn-sm btn-success">
                                                        <a href="{{url('edit-category/' . $category->catid)}}" style="color:#ffffff;"> <i class="fa fa-edit"></i></a>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger"><a href="{{url('delete-category/' . $category->catid)}}" style="color:#ffffff;"> <i class="fa fa-trash"></i></a></button>
                                            </td>
                                        </tr>
                                        <?php $count ++; ?>
                                        @endforeach
                                    @endif
                                          
                                    </tbody>
                                </table>
                    
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')

        </section>
        <!-- ============================================================== -->
		<!-- 						Content End 	 						-->
		<!-- ============================================================== -->


        
@endsection 