@extends('layouts.admin')

@section('content')




<!-- ============================================================== -->
<!-- 						Content Start	 						-->
<!-- ============================================================== -->
        
<div class="row page-header">
<div class="col-lg-6 align-self-center ">
    <h2>Add Products</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Products</a></li>
        <li class="breadcrumb-item active">Add</li>		
    </ol>
</div>
</div>

<section class="main-content">

<div class="row">
    @include('notification')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header card-default">
                Form elements
            </div>
            <div class="card-body">
                <form method="post" id="form" action="{{ url('/add-category') }}">
                    {{ csrf_field() }}
                    <div class="form-group ">
                        <label>Name</label>
                            <input type="text" class="form-control form-control-rounded" name="name">
                    </div>
                
                    <div class="form-group">
                        <label>Description</label>
                            <textarea name="description" id="" cols="10" rows="30" ></textarea>
                            <small class="text-muted">Give a short description for category.</small>
                    </div>
                        
                    <button type="submit" class="btn btn-primary margin-l-5 mx-sm-3">Add</button>
                    
                    
                </form>
            </div>
        </div>
    </div>
</div>
</section>
@endsection