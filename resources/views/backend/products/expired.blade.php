@extends('layouts.sub')

@section('content')




        <!-- ============================================================== -->
		<!-- 						Content Start	 						-->
		<!-- ============================================================== -->
					
            <div class="row page-header"><div class="col-lg-6 align-self-center ">
			  <h2>Products</h2>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Products</a></li>
					<li class="breadcrumb-item active">Manage</li>
				</ol></div></div>

        <section class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                            @include('notification')
					<div class="card-header card-default">
							Products
							<p class="text-muted">list of available medicines</p>
                        </div>
                        <div class="card-body">
                      
                                <table id="datatable2" class="table table-striped dt-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th style="width:80px">
                                                <strong>S/N</strong>
                                            </th>
                                            <th>
                                                <strong>PRODUCT NAME</strong>
                                            </th>
                                            <th>
                                                <strong>CATEGORY</strong>
                                            </th>
                                            <th>
                                                <strong>PRICE</strong>
                                            </th>
                                            <th>
                                                <strong>MANUFACTURER</strong>
                                            </th>
                                            <th>
                                                <strong>EXPIRY DATE</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>BATCH NO.</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>ACTION</strong>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                @if(count($products) == 0)
                                    <div>No Expired Products available</div>
                                @endif
                                    @if(count($products) > 0)
                                        <?php $count = 1; ?>

                                        @foreach($products as $product)
                                            
                                        <tr>
                                            <td>       
                                                    #<?php echo $count;?>.
                                            </td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->category->name }}</td>
                                            <td>&#8373 {{ $product->price }}</td> 
                                            <td>{{ $product->manufacturer }}</td>
                                            <td>{{ $product->expiry_date }}</td>
                                            <td class="text-center">
                                                <span class="badge badge-default">{{ $product->batch_no }}</span>
                                            </td>
                                            <td class="text-center">
                                                {{--  <button type="button" class="btn btn-sm btn-default">
                                                    <em class="fa fa-search"></em>
                                                </button>  --}}
                                                {{--  <button type="button" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></button>  --}}
                                                
                                                <button type="button" class="btn btn-sm btn-danger"><a href="{{url('delete-product/' . $product->pid)}}" style="color:#ffffff;"> <i class="fa fa-trash"></i></a></button>
                                            </td>
                                        </tr>
                                        <?php $count ++; ?>
                                        @endforeach
                                    @endif
                                          
                                    </tbody>
                                </table>
                    
                        </div>
                    </div>
                </div>
            </div>

            @include('footer')

        </section>
        <!-- ============================================================== -->
		<!-- 						Content End 	 						-->
		<!-- ============================================================== -->


        
@endsection 