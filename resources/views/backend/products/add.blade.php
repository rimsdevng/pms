@extends('layouts.admin')

@section('content')




<!-- ============================================================== -->
<!-- 						Content Start	 						-->
<!-- ============================================================== -->
        
<div class="row page-header">
<div class="col-lg-6 align-self-center ">
    <h2>Add Products</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Products</a></li>
        <li class="breadcrumb-item active">Add</li>		
    </ol>
</div>
</div>

<section class="main-content">


    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                    @include('notification')

                <div class="card-header card-default">
                   Add Product Information

                </div>
                <div class="card-body">
                    <form id="example-form" action="{{ url('add-product') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div>
                            <h3>Basic Product Information</h3>
                            <section>
                                <label for="name">Name *</label>
                                <input id="name" name="name" type="text" value="{{ old('name') }}"  class="required">

                                <label for="manufacturer">Manufacturer *</label>
                                <input id="manufacturer" name="manufacturer" type="text" value="{{ old('manufacturer') }}" class="required">

                                <label for="made_in">Made In *</label>
                                <input id="made_in" name="made_in" type="text" value="{{ old('made_in') }}">

                                <label for="made_in">Description</label>
                                <textarea name="key_features" id="key_features" cols="30" rows="10"></textarea>
                            </section>
                            
                            <h3>More Description</h3>
                            <section>
                               
                                <select class="form-control required" name="catid">
                                    @foreach($categories as $category)
                                        <option value="{{$category->catid}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                <label for="price">Price *</label>
                                <input id="price" name="price" type="number" class="required">


                                <label for="components">Components</label>
                                <input id="components" name="components" type="text">
                               
                                
                                <label for="instructions">Usage Instructions</label>
                                <input id="instructions" name="instructions" type="text">
                            </section>
                            <h3>Production Info</h3>

                            <section>
                                    
                            <label for="mfg_date">Manufacture Date</label>
                            <input id="mfg_date" name="mfg_date" type="date">

                           


                            <label for="expiry_date">Expiry Date</label>
                            <input id="expiry_date" name="expiry_date" max="{{ \Carbon\Carbon::tomorrow()->format('Y-M-D') }}" type="date">

                            <label for="batch_no">Batch Number</label>
                            <input id="batch_no" name="batch_no" type="text">
            
                                
                            </section>
                            
                            <h3>Finish</h3>
                            <section>
                            <label>Upload Product Image (Optional)</label>
                            <input type="file" name="images[]">
                            <br>
                            <p>Navigate back to correct any changes or proceed to save product to the system</p>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    


</section>
<!-- ============================================================== -->
<!-- 						Content End	 						-->
<!-- ============================================================== -->
@endsection