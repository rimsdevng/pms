   <!-- ============================================================== -->
		<!-- 						Navigation Start 						-->
		<!-- ============================================================== -->
        <div class="main-sidebar-nav default-navigation">
            <div class="nano">
                <div class="nano-content sidebar-nav">
				
					<div class="card-body border-bottom text-center nav-profile">
						<div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                        <img alt="profile" class="margin-b-10  " src="{{ url('backend/assets/img/avtar-2.png') }}" width="80">
                        <p class="lead margin-b-0 toggle-none"> {{ Auth::user()->name }} <span class="caret"></span></p>
                        <p class="text-muted mv-0 toggle-none">Welcome</p>						
                    </div>
					
                    <ul class="metisMenu nav flex-column" id="menu">
                        <li class="nav-heading"><span>MAIN</span></li>
						<li class="nav-item active"><a class="nav-link" href="{{ url('/home') }}"><i class="fa fa-home"></i> <span class="toggle-none">Dashboard <span class="badge badge-pill float-right mr-2"></span></span></a></li>						
                       
                        {{-- <li class="nav-item">
                            <a class="nav-link"  href="javascript: void(0);" aria-expanded="false"><i class="fa fa-th-large"></i> <span class="toggle-none">Widgets <span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column " aria-expanded="false">
								<li class="nav-item"><a class="nav-link" href="widgets-apps.html">Widgets Apps</a></li>
                                <li class="nav-item"><a class="nav-link" href="widgets-data.html">Widgets Data</a></li>
                            </ul>
                        </li>

						 <li class="nav-item">
                            <a class="nav-link"  href="javascript: void(0);" aria-expanded="false"><i class="fa fa-codepen"></i> <span class="toggle-none">Apps <span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
								<li class="nav-item"><a class="nav-link" href="app-calendar.html">Calendar</a></li>
                                <li class="nav-item"><a class="nav-link" href="app-chat.html">Chat App</a></li>
								<li class="nav-item"><a class="nav-link" href="app-ticket.html">Support Ticket</a></li>
								<li class="nav-item"><a class="nav-link" href="app-contact.html">Contact Grid</a></li>
								<li class="nav-item"><a class="nav-link" href="app-contact-detail.html">Contact Detail</a></li>
                            </ul>
                        </li>
						<li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-envelope-open"></i> <span class="toggle-none">Inbox<span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
                                <li class="nav-item"><a class="nav-link" href="mail-index.html">Inbox</a></li>
                                <li class="nav-item"><a class="nav-link" href="mail-view.html">Inbox view</a></li>
                                <li class="nav-item"><a class="nav-link" href="mail-compose.html">Compose</a></li>
                            </ul>
                        </li> --}}

                        <li class="nav-heading"><span>TRANSACTIONS</span></li>

                        <li class="nav-item">
                            <a  class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-shopping-cart"></i> <span class="toggle-none">Sales <span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
								<li class="nav-item"><a class="nav-link" href="{{ url('/shop') }}">POS</a></li>
								<li class="nav-item"><a class="nav-link" href="{{ url('/manage-sales') }}">Transactions</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}">Manage</a></li>
                                
                            </ul>
                        </li>						
                        
                        {{-- <li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-file"></i> <span class="toggle-none">Suppliers <span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
                                <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}">Add</a></li>
								<li class="nav-item"><a class="nav-link" href="{{ url('/home') }}">Manage</a></li>
								
                            </ul>
                        </li> --}}
                       
                        <li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-table"></i> <span class="toggle-none">Medicals <span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
                                {{--  <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}">Manage</a></li>  --}}
                                {{-- <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}">Out Of Stock</a></li> --}}
								<li class="nav-item"><a class="nav-link" href="{{ url('/expired-products') }}">Expired Drugs</a></li>
                                {{--  <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}">Instock</a></li>  --}}
                            </ul>
                        </li>
						
						<li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-bar-chart"></i> <span class="toggle-none">Reports <span class="fa arrow"></span>  <span class="badge badge-pill float-right mr-2"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
                                <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}">Sales Report</a></li>
                                {{-- <li class="nav-item"><a class="nav-link" href="{{ url('/home') }}">Export Products Inventory</a></li> --}}
                                {{--  <li class="nav-item"><a class="nav-link" href="chart-radial.html">Invoice</a></li>  --}}
                                
                            </ul>
                        </li>
						
                        {{-- <li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-map-marker"></i> <span class="toggle-none">Maps <span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
                                <li class="nav-item"><a class="nav-link" href="map-vector.html">Vector map</a></li>
                                <li class="nav-item"><a class="nav-link" href="map-google.html">Google map</a></li>
                            </ul>
                        </li> --}}
						
						<li class="nav-heading"><span>INVENTORY</span></li>
						<li class="nav-item">
                            <a class="nav-link"  href="javascript: void(0);" aria-expanded="false"><i class="fa fa-folder"></i> <span class="toggle-none">Drugs<span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
								<li class="nav-item"><a class="nav-link" href="{{ url('/add-product') }}">Add Product</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ url('/manage-products') }}">All Products</a></li>
                                {{--  <li class="nav-item"><a class="nav-link" href="{{ url('/shop') }}">Orders</a></li>  --}}
                            </ul>
                        </li>						
						<li class="nav-item">
                            <a class="nav-link"  href="javascript: void(0);" aria-expanded="false"><i class="fa fa-briefcase"></i> <span class="toggle-none">Categories <span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
								<li class="nav-item"><a class="nav-link" href="{{ url('add-category') }}">Add Category</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ url('/manage-categories') }}">All Categories</a></li>
                            </ul>
                        </li>
						{{--  <li class="nav-item">
                            <a class="nav-link"  href="javascript: void(0);" aria-expanded="false"><i class="fa fa-user-plus"></i> <span class="toggle-none">Users Management<span class="fa arrow"></span></span></a>
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
                                <li class="nav-item"><a class="nav-link" href="examples-add-and-update.html">Add and Update With Php</a></li>
								<li class="nav-item"><a class="nav-link" href="examples-delete.html">Delete With Php</a></li>
                            </ul>
                        </li>  --}}
						
                        <li class="nav-heading"><span>OTHERS</span></li>
						
                        <li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-cogs"></i> <span class="toggle-none">Settings <span class="fa arrow"></span></span></a>  
                            <ul class="nav-second-level nav flex-column sub-menu" aria-expanded="false">
                                  <li class="nav-item"><a class="nav-link" href="{{ url('/get-change-password') }}">Change Password</a></li>
                                {{--<li class="nav-item"><a class="nav-link" href="page-login.html">Login</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-register.html">Register</a></li>  --}}
                                {{-- <li class="nav-item"><a class="nav-link" href="page-forget-password.html">Forget Password</a></li>
								<li class="nav-item"><a class="nav-link" href="page-lockscreen.html">Lock-screen</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-404.html">404</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-gallery.html">Gallery</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-timeline.html">Timeline</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-invoice.html">Invoice</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-profile.html">Profile</a></li>

                                <li class="nav-item"><a class="nav-link" href="page-faqs.html">Faqs</a></li>
                                <li class="nav-item"><a class="nav-link" href="page-pricing.html">Pricing</a></li> --}}
                            </ul>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" aria-expanded="false"><i class="fa fa-folder-open"></i> <span class="toggle-none">Multi Level<span class="fa arrow "></span></span></a>
                            <ul class="nav-second-level nav flex-column" aria-expanded="false">
                                <li class="nav-item"><a class="nav-link" href="javascript: void(0);">Level 1</a></li>
                                <li class="nav-item"><a class="nav-link"href="javascript: void(0);" aria-expanded="false">Level 2 <span class="fa arrow"></span></a>
                                    <ul class="nav-third-level nav flex-column sub-menu" aria-expanded="false">
                                        <li class="nav-item"><a class="nav-link" href="javascript: void(0);">Level 2</a></li>
                                        <li class="nav-item"><a class="nav-link" href="javascript: void(0);">Level 2</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
		<!-- 						Navigation End	 						-->
		<!-- ============================================================== -->
