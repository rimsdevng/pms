@extends('layouts.admin')

@section('content')

    <style>

        .cart-total {
            margin-top: 10px;
        }
        .cart-total ul{
            display:inline-block;
        }
        .cart-total ul li {
            display: inline-block;
            position:relative;
        }
        .cart-total ul li a i {
            color: #333;
            font-size: 18px;
        }
        .cart-total ul li a span.product-number {
            color: #333;
            margin: 0 4px;
            transition:.3s;
        }
        .cart-total ul li a span.product-number:hover{
            color:#59b210;
        }
        .cart-total ul li a span.cart-count {
            color:#59b210;
        }
        .checkout {
            background: white none repeat scroll 0 0;
            box-shadow: 0 3px 5px rgba(0, 0, 0, 0.3);
            padding: 26px 10px;
            top: 150%;
            transition: all 0.3s ease 0s;
            transition:.3s;
        }
        .cart-total ul.cart-menu:hover li .checkout{
            opacity:1;
            top:100%;
            visibility:visible;
        }
        .checkout .cart-list {
            border-top: 1px solid #aaa;
            overflow: hidden;
            padding: 15px 0;
        }
        .checkout .cart-list .cart-img {
            float: left;
            width: 30%;
        }
        .checkout .cart-list .cart-info {
            float: left;
            text-align: left;
            width: 60%;
            padding-left: 10px;
        }
        .checkout .cart-list .cart-info h4 {
            margin-bottom:0;
        }
        .checkout .cart-list .cart-info h4 a {
            color: black;
            font-size: 16px;
            font-weight: 400;
            text-decoration: none;
            text-transform: capitalize;
        }
        .checkout .cart-list .cart-info .cart-price span {
            color: black;
            font-size: 16px;
        }
        .checkout .cart-list .cart-info .cart-price span.price {
            font-size: 18px;
        }
        .checkout .cart-list .pro-del {
            float: left;
            width: 10%;
        }
        .checkout .cart-list .pro-del a i {
            color: black;
            font-size: 17px;
        }
        .checkout .mini-cart-total {
            border-bottom: 1px solid #aaa;
            border-top: 1px solid #aaa;
            overflow: hidden;
            padding: 10px 0;
        }
        .checkout .mini-cart-total span {
            color: black;
            float: left;
            font-size: 17px;
            text-transform: capitalize;
        }
        .checkout .mini-cart-total span.total-price {
            float: right;
            font-size: 17px;
        }
        .checkout .cart-button {
            padding: 20px 0;
        }
        .checkout .cart-button a{
            background: black;
            float:left;
            color:white;
        }
        .checkout .cart-button a:hover{
            background:#666666;
            color:#fff;
            cursor:pointer;
        }
        .checkout .cart-button a.right,.checkout .cart-button a {
            display: block;
            font-weight: 700;
            text-transform: uppercase;
            text-decoration: none;
            padding: 5px 15px;
            font-size: 14px;
        }
        .checkout .cart-button a.right{
            background: black;
            float: right;
            color:white;
        }
        .checkout .cart-button a.right:hover{
            background:#333;
            color:white;
        }
    </style>
    <!-- product-area-start -->
    <div class="product-area ptb-50">
        <div class="container">
            <div class="row checkout col-md-8 col-md-offset-2">
                @if(session()->has('cart') && count(session()->get('cart')) > 0)
                    @foreach(session()->get('cart') as $item)
                        <div class="cart-list">

                            <div class="cart-img">
                                <a href="{{url('product/' . $item->slug)}}" title="">
                                    @if(isset($item->Images[0]))
                                        <img src="{{$item->Images[0]->url}}" />
                                    @else
                                        <img src="{{url('img/placeholder.png')}}" />
                                    @endif
                                </a>
                            </div>
                            <div class="cart-info">
<h4><a href="#">{{$item->name}}</a></h4>
                                <div class="cart-price">
 <span>{{$item['quantity']}} x<span class="price"> &#x20A6;{{number_format($item->price)}} </span> </span>
                                </div>
                            </div>
                            <div class="pro-del">
                                <a href="{{url('remove-from-cart/'. $item->pid)}}"><i class=" pe-7s-close-circle"></i></a>
                            </div>
                        </div>
                    @endforeach

                    @else
                    <p align="center">No items in cart</p>
                @endif

                <div class="mini-cart-total">
                    <span>Subtotal</span>
                    <span class="total-price">&#x20A6;{{number_format(session()->get('carttotal'))}}</span>
                </div>

                @if(session()->has('cart') && count(session()->get('cart')) > 0)
                    <form id="form" action="{{url('sales')}}" method="post">

                        @csrf

                        <div class="form-group">
                        <label class="col-md-4 control-label">Address</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="address"></textarea>
                            </div>
                        </div>

                            <div class="cart-button">
                                <a  onclick="document.getElementById('form').submit()" title="Checkout" class="right">Place Order</a>
                            </div>
                    </form>
                @endif


            </div>
        </div>
    </div>
    <!-- product-area-end -->

    <!-- service-area-2-start -->

    

@endsection