@extends('layouts.sub')

@section('content')
<style>
        div.dataTables_wrapper div.dataTables_filter input{
 
            width: 600px !important;
        }
        
</style>
		<!-- ============================================================== -->
		<!-- 						Content Start	 						-->
		<!-- ============================================================== -->
		
            <div class="row page-header"><div class="col-lg-6 align-self-center ">
			  <h2>Products</h2>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
					<li class="breadcrumb-item"><a href="#">E-Commerce</a></li>
					<li class="breadcrumb-item active">Products</li>
				</ol></div></div>

        <section class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
					 <div class="card-header card-default">
							<div class="float-right mt-10">
								<a href="{{ url('add-product') }}" class="btn btn-primary btn-rounded box-shadow btn-icon"><i class="fa fa-plus"></i> Add New Product</a>
                            </div>
							Products
                            <p class="text-muted">Make a simple Sale</p>
                            





<div class="row">
        <div class='col-md-6'>
        


                <div role="tabpanel" class="tab-pane" id="profile">
                        <div class="widget white-bg">
                             <div class="row">
                                @if(session()->has('cart') && count(session()->get('cart')) > 0)
                                @foreach(session()->get('cart') as $item)
                        
                                     <div class="col-md-6 col-xs-6 b-r"> <strong>Product:</strong>
                                         
                                         <p class="text-muted">{{$item->name}}</p>
                                     </div>
                                     <hr>
                                     @if(session()->get('country') != "Ghana")
                                     <div class="col-md-4 col-xs-6 b-r"> 
                                         <strong>Qty X Price (&#8373)</strong>
                                         <br>
                                         
                                        <p class="text-muted">{{$item['quantity']}} x<span class="price"> &#8373;{{number_format($item->price)}} </span>
                                        </p>
                                        @else
                                        <p class="text-muted">{{$item['quantity']}} x<span class="price"> ${{number_format(($item->price / $rate) )}} </span> </p>
                                        @endif
                                     </div>
                                     <a href="{{url('remove-from-cart/'. $item->pid)}}">Remove</a>
                                     <br>
                                @endforeach
                                @endif
                             </div>
                             
                             <hr>
                            </div>
                     </div>
                     




    {{-- This is where it starts --}}
    
    
    {{-- This is where it ends --}}
            
        </div>
        <div class="col-md-4 col-md-offset-2">
            <table class="table table-striped text-right">
                <tbody>
                    @if(session()->has('cart') && count(session()->get('cart')) > 0)
                    <tr>
                    <td><strong>Sub Total :</strong></td>
                    <td>&#8373;{{number_format(session()->get('carttotal'))}}</td>
                    </tr>
                    <tr>
                        <td><strong>Total Item :</strong></td>
                        <td> {{count(session()->get('cart'))}}</span> item(s) -</span> </td>
                    </tr>
                    <tr>
                        <td><strong>TOTAL :</strong></td>
                        <td>&#8373;{{number_format(session()->get('carttotal'))}}</td>
                    </tr>
                    @else 
                        <tr>
                            <td>No Item in cart</td>
                        </tr>
                    @endif 
                </tbody>
            </table>
            <br>
            <div class="float-right mt-10">
                @if(session()->has('cart') && count(session()->get('cart')) > 0)
                <a href="{{url('check-out')}}" class="btn btn-primary btn-rounded box-shadow btn-icon"><i class="fa fa-shopping-cart"></i> Checkout</a>

                    @foreach(session()->get('cart') as $item)
            
                    <span><a href="{{url('remove-from-cart/'. $item->pid)}}" class="btn btn-warning btn-rounded box-shadow"><i class="fa fa-trash"></i> Clear</a></span>
                    @endforeach

                    @else
                    <span><i class="fa fa-shopping-cart" title="Empty Shopping cart"></i></span>
                @endif
            </div>
        </div>
    </div>

    </div>
						
                        <div class="card-body">
                                <table id="datatable1" class="table table-striped dt-responsive nowrap table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                <strong>S/N</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>Name</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>Components</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>Price</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>Expiry Date</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>Add To Cart</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>Action</strong>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                            @if(count($products) == 0)
                                <div>No Products available</div>
                            @endif
                                
                            @if(count($products) > 0)
                                <?php $count = 1; ?>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>#<?php echo $count;?>.</td>
                                            <td>{{$product->name}}</td>
                                            <td>{{ $product->components }}</td>
                                            <td>&#8373 {{ $product->price }}</td>
                                            <td>{{ $product->expiry_date }}</td>
                                            <td class="text-center">
                                                <span class="btn btn-info margin-r-5"> <a href="{{url('add-to-cart/' . $product->pid)}}" style="color:#ffffff;">Add to Cart</a></span> 

                                                <a href="#" class="btn btn-warning margin-r-5" data-toggle="modal" data-target="#loginModal" >+Qty</a>                         
                                            </td>
                                            <td class="text-center">
                                                
                                                    {{--  <button type="button" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></button>  --}}
                                               
                                                    <button type="button" class="btn btn-sm btn-success">
                                                            <a href="{{url('edit-product/' . $product->pid)}}" style="color:#ffffff;"> <i class="fa fa-edit"></i></a>
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-danger"><a href="{{url('delete-product/' . $product->pid)}}" style="color:#ffffff;"> <i class="fa fa-trash"></i></a></button>
                                               
                                                
                                            </td>
                                        </tr>
                                        

                                        @endforeach
                                    @endif
                                        
                                    </tbody>
                                </table>
                                
                        </div>
                    </div>
                </div>
            </div>

            
                               
    <div class="modal fade" id="loginModal">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times"></span></button>
                  <h5 class="modal-title" id="exampleModalLabel">Login Modal</h5>
                </div>
                <div class="modal-body">
                      {{--  <form role="form">
                          <div class="form-group">
                              <input type="email" class="form-control" placeholder="Username">
                          </div>
                          <div class="form-group">
                              <input type="password" class="form-control" placeholder="Password">
                          </div>
                          <div class="clearfix">
                              <div class="checkbox checkbox-inline checkbox-primary">
                                              <input id="checkbox11" type="checkbox" checked="">
                                              <label for="checkbox11"> Option one </label>
                                          </div>
                              <button type="button" class="btn  btn-primary float-right">Login</button>
                          </div>
                      </form>  --}}

                      
                      <form action="{{url('add-to-cart/' . $product->pid)}}">
                        <div class="btn btn-sm btn-teal">
                            <input class="qty" type="number" min="0" value="1" name="quantity" id="quantity"/>
                            <input type="button" value="+" data-max="1000" onclick="document.getElementById('quantity').value++;" class="plus" />
                            <input type="button" value="-" data-min="1" onclick="document.getElementById('quantity').value--;"  class="minus" />
                        </div>
                        <button type="submit">Add to Cart</button>
                    </form>

                      <hr>
                      <div class="text-center">
                          <p>To increase quantity</p>
                          <p>Kindly Press the "+" to Add or "-" to Decrease</p>
                      </div>
                </div>
              </div>
            </div>
          </div>
            @include('footer')

        </section>
        <!-- ============================================================== -->
		<!-- 						Content End 	 						-->
		<!-- ============================================================== -->

@endsection 