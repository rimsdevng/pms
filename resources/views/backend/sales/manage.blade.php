@extends('layouts.sub')

@section('content')




        <!-- ============================================================== -->
		<!-- 						Content Start	 						-->
		<!-- ============================================================== -->
					
            <div class="row page-header"><div class="col-lg-6 align-self-center ">
			  <h2>Sales</h2>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Sales</a></li>
					<li class="breadcrumb-item active">Manage</li>
				</ol></div></div>

        <section class="main-content">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                            @include('notification')
					<div class="card-header card-default">
							Sales
							<p class="text-muted">list of sales transactions</p>
                        </div>
                        <div class="card-body">
                      
                                <table id="datatable2" class="table table-striped dt-responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th style="width:80px">
                                                <strong>S/N</strong>
                                            </th>
                                            <th>
                                                <strong>SALES ID</strong>
                                             </th>
                                            <th>
                                                <strong>STAFF NAME</strong>
                                            </th>
                                            <th>
                                                <strong>TOTAL</strong>
                                            </th>
                                            <th>
                                                <strong>DATE SOLD</strong>
                                            </th>
                                            {{--  <th>
                                                <strong>MANUFACTURER</strong>
                                            </th>
                                            <th>
                                                <strong>EXPIRY DATE</strong>
                                            </th>
                                            <th class="text-center">
                                                <strong>BATCH NO.</strong>
                                            </th>  --}}
                                            <th class="text-center">
                                                <strong>ACTION</strong>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                @if(count($sales) == 0)
                                    <div>No sales available</div>
                                @endif
                                    @if(count($sales) > 0)
                                        <?php $count = 1; ?>
                                        

                                        @foreach($sales as $sale)
                                        <tr>
                             
                                                <td>
                                                    
                                                   #<?php echo $count;?>.

                                                </td>
                                                <td>SID - {{ $sale->sid}} </td>
                                                <td>{{$sale->User->name}}</td>
                                                <td>&#8373;{{$sale->total}}</td>
                                                <td>{{$sale->created_at->toDayDateTimeString()}}</td>
                                                <td>
                                                    <a href="{{url('sale/' . $sale->sid)}}" title="View" target="_blank" class="btn btn-primary">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                         <button type="button" class="btn btn-sm btn-danger"><a href="{{url('delete-sale/' . $sale->sid)}}" style="color:#ffffff;"> <i class="fa fa-trash"></i></a></button>  
                                                </td>
                                        </tr>
                                        <?php $count ++; ?>

                                        @endforeach









                                    @endif
                                          
                                    </tbody>
                                </table>
                    
                        </div>
                    </div>
                </div>
            </div>

            

            @include('footer')

        </section>
        <!-- ============================================================== -->
		<!-- 						Content End 	 						-->
		<!-- ============================================================== -->


        
@endsection 