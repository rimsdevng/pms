<!-- mini-cart-total-start -->
                    <div class="cart-total text-right">
                        <ul class="cart-menu">
                            <li>

                                <a href="#">
                                    <i class="fa fa-shopping-cart"></i>
                                </a>

                                <div class="shopping-cart">
                                    @if(session()->has('cart') && count(session()->get('cart')) > 0)
                                        @foreach(session()->get('cart') as $item)
                                            <div class="cart-list">

                                                <div class="cart-img">
                                                    <a href="{{url('product/' . $item->slug)}}" title="">
                                                        @if(isset($item->Images[0]))
                                                        <img src="{{$item->Images[0]->url}}" />
                                                            @else
                                                            <img src="{{url('img/placeholder.png')}}" />
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="cart-info">
                                                    <h4><a href="#">{{$item->name}}</a></h4>
                                                    <div class="cart-price">
                                                        @if(session()->get('country') != "Ghana")
                                                        <span>{{$item['quantity']}} x<span class="price"> &#x20A6;{{number_format($item->price)}} </span> </span>
                                                        @else
                                                        <span>{{$item['quantity']}} x<span class="price"> ${{number_format(($item->price / $rate) )}} </span> </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="pro-del">
                                                    <a href="{{url('remove-from-cart/'. $item->pid)}}"><i class=" pe-7s-close-circle"></i></a>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif

                                    <div class="mini-cart-total">
                                        <span>Subtotal</span>
                                        <span class="total-price">&#x20A6;{{number_format(session()->get('carttotal'))}}</span>
                                    </div>
                                    <div class="cart-button">
                                        <a href="{{url('check-out')}}" title="Checkout" class="right">Checkout</a>
                                    </div>
                                </div>
                            </li>
                            @if(session()->has('cart') && count(session()->get('cart')) > 0)
                            <li><a href="#"><span class="product-number"><span class="sell">{{count(session()->get('cart'))}}</span> item(s) -</span> </a></li>
                            <li><a href="#"><span class="cart-count">&#x20A6;{{number_format(session()->get('carttotal'))}}</span></a></li>
                            @endif
                        </ul>
                    </div>
                    <!-- mini-cart-end -->



