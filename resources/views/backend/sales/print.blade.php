@extends('layouts.sub')

@section('content')

        <!-- ============================================================== -->
		<!-- 						Content Start	 						-->
		<!-- ============================================================== -->
					
            <div class="row page-header"><div class="col-lg-6 align-self-center ">
			  <h2>Invoice</h2>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active">Invoice</li>
				</ol></div></div>

        <section class="main-content">

            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Invoice No :<small>85659-2018</small></h4>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-right">
                                        <button class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Edit </button>
                                        <button class="btn btn-default btn-sm"><i class="fa fa-check "></i> Save </button>
                                        <button class="btn btn-primary btn-sm"><i class="fa fa-money"></i> Make A Payment</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row margin-b-40">
                                <div class="col-sm-6">
                                    <h4>85659-2018</h4>

                                    <address>
                                        <strong>Dansoman, Branch.</strong><br>
                                        Dansoman district<br>
                                        Accra, Ghana<br>
                                        <abbr title="Phone">Pbx:</abbr> +233 23-856-5890
                                    </address>
                                </div>
                              
                            </div>

                            <div class="table-responsive margin-b-40">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Item List</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Tax</th>
                                            <th>Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(session()->has('cart') && count(session()->get('cart')) > 0)
                                        @foreach(session()->get('cart') as $item)
                                            <tr>
                                                <td>
                                                    <div><strong>{{$item->name}}</strong></div>
                                                    <small>
                                                            {{$item->components}}
                                                    </small>
                                                </td>
                                                <td>{{$item['quantity']}} x</td>
                                                <td>&#8373;{{number_format($item->price)}}</td>
                                                <td>&#8373; 0.00 </td>
                                                <td>&#8373;{{number_format($item->price)}}</td>
                                            </tr>

                                        @endforeach

                                        @else
                                            <p align="center">No items in cart</p>
                                        @endif
                    

                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class='col-md-6'>
                                    <h5><b>Additional Information: </b></h5>
                                    <p>
                                            Now you can collect you order over the counter. Please, kindly keep your receipt copy for future reference purpose. 
                                                    Management will not be hold responsible for abuse of product.
                                            <small>
                                              Terms and condition. 
                                            </small>
                                    </p>
                                </div>
                                <div class="col-md-4 col-md-offset-2">
                                    <table class="table table-striped text-right">
                                        <tbody>
                                            <tr>
                                                <td><strong>Sub Total :</strong></td>
                                                <td>&#8373;{{number_format(session()->get('carttotal'))}}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>TAX :</strong></td>
                                                <td>&#8373; 0.00</td>
                                            </tr>
                                            <tr>
                                                <td><strong>TOTAL :</strong></td>
                                                <td>&#8373;{{number_format(session()->get('carttotal'))}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">

                                        @if(session()->has('cart') && count(session()->get('cart')) > 0)
                                        <form id="form" action="{{url('sales')}}" method="post">
                    
                                                @csrf
                                    <div>
                                        <button class="btn btn-success" onclick="window.print();"><i class="fa fa-print"></i> Print</button>            
                                        
                                                  
                                        <div class="btn btn-default">
                                                <a  onclick="document.getElementById('form').submit()" title="Checkout" class="right">Complete Sales</a>
                                        </div>
                                    </div>

                                </form>
                                @endif
                                </div>
                            </div>



                            
                                
        
                                {{--  <div class="form-group">
                                <label class="col-md-4 control-label">Address</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" name="address"></textarea>
                                    </div>
                                </div>  --}}
        
                                    
                            {{--    --}}

                        </div>
                    </div>
                </div>
            </div>

            
@include('footer')


        </section>
        <!-- ============================================================== -->
		<!-- 						Content End		 						-->
		<!-- ============================================================== -->

@endsection 