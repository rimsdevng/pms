<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>GhanaPlus Pharmacy Management System</title>

        <!-- Common Plugins -->
        <link href="{{ url('backend/assets/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
		
        <!-- DataTables -->
        <link href="{{ url('backend/assets/lib/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('backend/assets/lib/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css">

        <link href="{{ url('backend/assets/lib/datatables/buttons.dataTables.css') }}" rel="stylesheet" type="text/css">


        <!-- Custom Css-->
        <link href="{{ url('backend/assets/scss/style.css') }}" rel="stylesheet">
		
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- ============================================================== -->
        <!-- 						Topbar Start 							-->
        <!-- ============================================================== -->
        @include('backend.navs.topbar')
        
		<!-- ============================================================== -->
		<!--                        Topbar End                              -->
		<!-- ============================================================== -->
		
        
		
		<!-- ============================================================== -->
		<!--                        Right Side Start                        -->
        <!-- ============================================================== -->
        @include('backend.navs.sidebarRight')
		
        
        
		<!-- ============================================================== -->
		<!--                        Right Side End                          -->
		<!-- ============================================================== -->
		

        <!-- ============================================================== -->
		<!-- 						Navigation Start 						-->
		<!-- ============================================================== -->
        @include('backend.navs.sidebarLeft')
        <!-- ============================================================== -->
		<!-- 						Navigation End	 						-->
		<!-- ============================================================== -->


        
		<!-- ============================================================== -->
		<!-- 						Content Start	 						-->
		<!-- ============================================================== -->
        
        @yield('content')

            
        <!-- ============================================================== -->
		<!-- 						Content End 	 						-->
		<!-- ============================================================== -->



        <!-- Common Plugins-->
        <script src="{{ url('backend/assets/lib/jquery/dist/jquery.min.js') }}"></script>
		<script src="{{ url('backend/assets/lib/bootstrap/js/popper.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/pace/pace.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/nano-scroll/jquery.nanoscroller.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/metisMenu/metisMenu.min.js') }}"></script>
        <script src="{{ url('backend/assets/js/custom.js') }}"></script>
		
        <!-- Datatables-->
        <script src="{{ url('backend/assets/lib/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/datatables/jszip.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ url('backend/assets/lib/datatables/buttons.html5.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('#datatable1').dataTable();
            });

            $(document).ready(function () {
                $('#datatable2').DataTable({
					 dom: 'Bfrtip',
					buttons: [
						'copyHtml5',
						'excelHtml5',
						'csvHtml5',
						'pdfHtml5'
					]
				});

            });
        </script>



        {{--  <script>
            
        </script>  --}}

        
    </body>

</html>