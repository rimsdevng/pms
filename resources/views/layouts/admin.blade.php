<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>GhanaPlus Pharmacy Management System</title>
        <link rel="icon" type="image/png" sizes="16x16" href="assets/img/icon.png">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
		
        <!-- Common Plugins -->
        <link href="{{ url('backend/assets/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
		
		<!-- Vector Map Css-->
        <link href="{{ url('backend/assets/lib/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
		
		<!-- Chart C3 -->
		<link href="{{ url('backend/assets/lib/chart-c3/c3.min.css') }}" rel="stylesheet">
		<link href="{{ url('backend/assets/lib/chartjs/chartjs-sass-default.css') }}" rel="stylesheet">

        <link href="{{ url('backend/assets/lib/toast/jquery.toast.min.css') }}" rel="stylesheet">
        <!-- DataTables -->
        <link href="{{ url('backend/assets/lib/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('backend/assets/lib/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
		
        <!-- Custom Css-->
        <link href="{{ url('backend/assets/scss/style.css') }}" rel="stylesheet">

        
        <!-- Jquery Steps-->
        <link rel="stylesheet" href="{{ url('backend/assets/lib/jquery-steps/jquery-steps.css') }}">
		


        {{-- <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]--> --}}
    </head>
    <body>

			<!-- ============================================================== -->
			<!-- 						 Start 							-->
            <!-- ============================================================== -->


            @include('backend.navs.topbar')
            {{-- @include('backend.navs.sidebarRight') --}}
            @include('backend.navs.sidebarLeft')
                @yield('content')
            

    
            <!-- ============================================================== -->
            <!-- 						 End		 						-->
            <!-- ============================================================== -->
    
            
            <!-- Common Plugins -->
            <script src="{{ url('backend/assets/lib/jquery/dist/jquery.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/bootstrap/js/popper.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/pace/pace.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/slimscroll/jquery.slimscroll.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/nano-scroll/jquery.nanoscroller.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/metisMenu/metisMenu.min.js') }}"></script>
            <script src="{{ url('backend/assets/js/custom.js') }}"></script>
                
            <!--Chart Script-->
            <script src="{{ url('backend/assets/lib/chartjs/chart.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/chartjs/chartjs-sass.js') }}"></script>
    
            <!--Vetor Map Script-->
            <script src="{{ url('backend/assets/lib/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/vectormap/jquery-jvectormap-us-aea-en.js') }}"></script>
            
            <!-- Chart C3 -->
            <script src="{{ url('backend/assets/lib/chart-c3/d3.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/chart-c3/c3.min.js') }}"></script>
        
            <!-- Datatables-->
        {{--  <script src="{{ url('backend/assets/lib/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/datatables/dataTables.responsive.min.js') }}"></script>  --}}
            <script src="{{ url('backend/assets/lib/toast/jquery.toast.min.js') }}"></script>
            <script src="{{ url('backend/assets/js/dashboard.js') }}"></script>
            

            <!-- Jquery Steps -->
            <script src="{{ url('backend/assets/lib/jquery-steps/jquery.steps.min.js') }}"></script>
            <script src="{{ url('backend/assets/lib/jquery-validate/jquery.validate.min.js') }}"></script>
   
            <!-- Datatables-->
        <script src="{{ url('backend/assets/lib/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('backend/assets/lib/datatables/dataTables.responsive.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('#datatable1').dataTable();
            });
        </script>
   
       <script>
        $("#example-basic").steps({
            headerTag: "h3",
            bodyTag: "section",
            
            autoFocus: true
        });
        
        $("#example-vertical").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: "vertical"
        });

        //steps with form
        var form = $("#example-form");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                {{--  alert("Submitted!");  --}}
                form.submit();

            }
        });
    </script>

        
    </body>
    
    </html>