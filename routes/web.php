<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('dashboard', 'HomeController@index')->name('dashboard');

Route::get('profile','HomeController@profile');

//product categories
Route::get('add-category','HomeController@addCategory');
Route::get('edit-category/{catid}','HomeController@editCategory');
Route::get('delete-category/{catid}','HomeController@deleteCategory');

Route::post('add-category','HomeController@postAddCategory');
Route::post('edit-category/{catid}','HomeController@postEditCategory');

Route::get('manage-categories','HomeController@getCategories');

//products
Route::get('add-product','HomeController@addProduct');
Route::get('edit-product/{pid}','HomeController@editProduct');
Route::get('delete-product/{pid}','HomeController@deleteProduct');
Route::get('manage-products','HomeController@manageProducts');

Route::post('add-product','HomeController@postAddProduct');
Route::post('edit-product/{pid}','HomeController@postEditProduct');

//settings
Route::get('settings','HomeController@settings');
Route::post('settings','HomeController@postSetting');

//sales
Route::get('manage-sales','HomeController@manageSales');
Route::get('delete-sale/{sid}','HomeController@deleteSales');
Route::get('sale/{sid}','HomeController@saleDetails');

Route::post('sales','HomeController@postSales');

//purchases
Route::get('purchases','HomeController@managePurchases');


//users
Route::get('manage-users','HomeController@users');
Route::get('edit-user/{uid}','HomeController@editUser');
Route::get('check-out','HomeController@checkout');
Route::get('pay/{sid}','HomeController@pay');


Route::post('edit-user/{uid}','HomeController@postEditUser');
Route::get('//get-change-password', 'HomeController@changePassword');
Route::post('/change-password','HomeController@postChangePassword');
//admins
Route::get('manage-admins','HomeController@manageAdmins');


//Public site
//Route::get('/','PublicController@index');
Route::get('/shop/{slug}','OrderController@categorySearch');


Route::get('/shop','OrderController@shop');
Route::get('product/{slug}','OrderController@productDetails');
Route::get('add-to-cart/{pid}','OrderController@addToCart');
Route::get('remove-from-cart/{pid}','OrderController@removeFromCart');

Route::get('/expired-products','HomeController@getExpiredProducts');




// Route::get('test-sales','OrderController@getTestSales');